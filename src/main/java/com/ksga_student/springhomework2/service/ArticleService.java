package com.ksga_student.springhomework2.service;

import com.ksga_student.springhomework2.model.Article;

import java.util.List;

public interface ArticleService {
    List<Article> findAll();
    Article findById(int id);
    boolean add(Article article);
    boolean edit(Article article);
    boolean remove(int id);
}

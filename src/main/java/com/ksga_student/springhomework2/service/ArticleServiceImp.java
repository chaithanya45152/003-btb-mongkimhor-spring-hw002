package com.ksga_student.springhomework2.service;

import com.ksga_student.springhomework2.model.Article;
import com.ksga_student.springhomework2.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ArticleServiceImp implements ArticleService{
    private ArticleRepository articleRepository;

    @Autowired
    public ArticleServiceImp(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @Override
    public List<Article> findAll() {
        return articleRepository.findAll();
    }

    @Override
    public Article findById(int id) {
        return articleRepository.findById(id);
    }

    @Override
    public boolean add(Article article) {
        return articleRepository.add(article);
    }

    @Override
    public boolean edit(Article article) {
        return articleRepository.edit(article);
    }

    @Override
    public boolean remove(int id) {
        return articleRepository.remove(id);
    }
}
